function loadRetourListenners() {
	$('#planning .card.bg-danger').click(function() {
		var idResa = $(this).attr('data-idResa');
		openRetourPopup(idResa);
		});
	}
	
function openRetourPopup(idResa) {
	$('#RetourPopUp').attr('data-idResa',idResa)
	$('#RetourPopUp').modal();
	}
	
$( document ).ready(function() {
	$('#RetourPopUp .btn-info').click(function () {
		var idResa = $('#RetourPopUp').attr('data-idresa');
		var kmDepart = $('#RetourPopUp #kmDepart').val();
		var kmArrivee = $('#RetourPopUp #kmFin').val();
		var pbTechnique = $('#RetourPopUp #pbTechnique').val();
		$.ajax({
			method: "PATCH",
			url: urlAPIVoiture + "reservations/" + loginToken + "/" + idResa + "?km=" + kmArrivee + "&kmDepart=" + kmDepart + "&pb=" + pbTechnique
			})
			.done(function(data){
				loadPlanning($('.bojeu #vehicules').val(),DDMMYYYY_2_YYYYMMDD($('.bojeu input').val()),null);
				$('#RetourPopUp').modal('hide');
				detectError(String(data),$('#RetourPopUp'));
				});
		});
	});