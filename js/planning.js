$( document ).ready(function() {
	function replaceAll(machaine, chaineARemaplacer, chaineDeRemplacement) {
		   return machaine.replace(new RegExp(chaineARemaplacer, 'g'),chaineDeRemplacement);
		 }
		
	$('.bojeu button[type="submit"]').click(function () {
		var vehicule = $('.bojeu #vehicules').val();
		var date = $('.bojeu input').val();
		
		var regex = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
		var test = regex.test(date);
		if(test)
			{
			var noJour = date.substr(0,2);
			var noMois = date.substr(3,2);
			var noAnnee = date.substr(6,4);
			date = noAnnee + "-" + noMois + "-" + noJour;
			loadPlanning(vehicule,date,null);
			}
			else
			{
			$('.bojeu input').css('background-color','#EA9EB0');
			}
		});
		
	$('.bojeu input').focus(function () {
		$('.bojeu input').css('background-color','#FFFFFF');
		});
		
	$('.goLeftPlanning').click(function() {
		var date = $('.bojeu input').val();
		var regex = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
		var test = regex.test(date);
		if(test)
			{
			stringDateOrigine = DDMMYYYY_2_YYYYMMDD(date);
			dateOrigine = new Date(stringDateOrigine);
				
			dateTimeOrigine = dateOrigine.getTime();
			newDateTime = dateTimeOrigine - (1000*60*60*24*7);
			newDate = new Date(newDateTime);
				
			var mm = newDate.getMonth() + 1; // getMonth() is zero-based
			var dd = newDate.getDate();

			var newDateString =([(dd>9 ? '' : '0') + dd,'/',(mm>9 ? '' : '0') + mm,'/',newDate.getFullYear()].join(''));
				
			console.log(newDateString);
			loadPlanning($('.bojeu #vehicules').val(),DDMMYYYY_2_YYYYMMDD(newDateString));
			$('.bojeu input').val(newDateString);
			}
		});
		
	$('.goRightPlanning').click(function() {
		var date = $('.bojeu input').val();
		var regex = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
		var test = regex.test(date);
		if(test)
			{
			stringDateOrigine = DDMMYYYY_2_YYYYMMDD(date);
			dateOrigine = new Date(stringDateOrigine);
				
			dateTimeOrigine = dateOrigine.getTime();
			newDateTime = dateTimeOrigine + (1000*60*60*24*7);
			newDate = new Date(newDateTime);
				
			var mm = newDate.getMonth() + 1; // getMonth() is zero-based
			var dd = newDate.getDate();

			var newDateString =([(dd>9 ? '' : '0') + dd,'/',(mm>9 ? '' : '0') + mm,'/',newDate.getFullYear()].join(''));
				
			console.log(newDateString);
			loadPlanning($('.bojeu #vehicules').val(),DDMMYYYY_2_YYYYMMDD(newDateString));
			$('.bojeu input').val(newDateString);
			}
		});
});

function loadPlanning(vehicule,date,done) {
	$.ajax({
		method: "GET",
		url: urlAPIVoiture + "jours/" + loginToken + "/"
		})
	.done(function(data){ 
		if(detectError(data,$('#RecherchePopUp')))
			{
			$('#RecherchePopUp').modal('hide');
			}
		var planning = '';
		$.ajax({
			method: "GET",
			url: urlAPIVoiture + "vehicules/" + loginToken + "/" + vehicule,
			data : 'date=' + date
			})
		.done(function(planning){ 
			detectError(planning);
			$('#planning').html('');
			var nbJour = 0;
			var matin, aprem;
			data.forEach(function (e) {
				matin = planning[nbJour*2];
				aprem = planning[(nbJour*2 + 1)];
				nbJour++;
				if(e.dispo == true)
					{
					
					if(matin.data == "null")
						{
						matin.data = '';
						}
						
					if(matin.etat == 'dispo')
						{
						matin.coul = 'bg-success';
						matin.dispo = 1;
						matin.cursor = ' style="cursor:pointer;"';
						}
					else if(matin.etat == 'reserve')
						{
						matin.coul = 'bg-danger';
						matin.dispo = 0; 
						matin.cursor = ' style="cursor:pointer;"';
						}
					else if(matin.etat == 'retour')
						{
						matin.coul = 'bg-info';
						matin.dispo = 0;
						matin.cursor = ' ';
						}
						
					if(aprem.data == "null")
						{
						aprem.data = '';
						}
						
					if(aprem.etat == 'dispo')
						{
						aprem.coul = 'bg-success';
						aprem.dispo = 1;
						aprem.cursor = ' style="cursor:pointer;"';
						}
					else if(aprem.etat == 'reserve')
						{
						aprem.coul = 'bg-danger';
						aprem.dispo = 0;
						aprem.cursor = ' style="cursor:pointer;"';
						}
					else if(aprem.etat == 'retour')
						{
						aprem.coul = 'bg-info';
						aprem.dispo = 0;
						aprem.cursor = ' ';
						}
					
					var listMois = new Array();
					listMois['01'] = "janvier";
					listMois['02'] = "février";
					listMois['03'] = "mars";
					listMois['04'] = "avril";
					listMois['05'] = "mai";
					listMois['06'] = "juin";
					listMois['07'] = "juillet";
					listMois['08'] = "août";
					listMois['09'] = "septembre";
					listMois['10'] = "octobre";
					listMois['11'] = "novembre";
					listMois['12'] = "décembre";
					
					// var date = new Date(matin.dateDebut);
					var noAnnee = matin.date.substr(0,4);
					var noMois = matin.date.substr(5,2);
					var noJour = matin.date.substr(8,2);
					
					$('#planning').append('<div class="col" data-date="' + noAnnee + '-' + noMois + '-' + noJour + '"><div class="row"><div class="col"><div class="card bg-light text-dark blanc"><div class="card-header">' + e.nom.substr(0,1).toUpperCase() +	e.nom.substr(1,e.nom.length) + ' ' + noJour + ' ' + listMois[noMois] + ' ' + noAnnee + '</div></div><div class="card ' +  matin.coul + ' text-white carteDJ" data-demiJ="0" data-dispo="' + matin.dispo + '" data-date="' + noAnnee + '-' + noMois + '-' +  noJour + '" ' + matin.cursor + '><div class="card-header">Matin</div><div class="card-body"><p class="card-text data"></p></div></div></div></div><div class="row"><div class="col"><div class="card ' +  aprem.coul+ ' text-white carteDJ" data-dispo="' + aprem.dispo + '" data-demiJ="1" data-date="' + noAnnee + '-' + noMois + '-' +  noJour + '" ' + aprem.cursor + '><div class="card-header">Après-Midi</div><div class="card-body"><p class="card-text data"></p></div></div></div></div></div>');
					checkData(matin,'0');
					checkData(aprem,'1');
					}
				if(done != null)
					{
					done();
					}
				});
			loadResaListenners();
			loadRetourListenners();
			});
		});
	}

function checkData(demiJ,idDemiJ){
	var noAnnee = demiJ.date.substr(0,4);
	var noMois = demiJ.date.substr(5,2);
	var noJour = demiJ.date.substr(8,2);
	if (demiJ.data.length != 0)
		{
		console.log(demiJ.data);
		var idResa = demiJ.data.id;
		var idConducteur = demiJ.data[0].conducteur;
		var idUser = demiJ.data[0].user;
		$('#planning .col[data-date="' + noAnnee + '-' + noMois + '-' + noJour + '"] [data-demiJ="' + idDemiJ + '"]').attr('data-idResa',idResa);
		$.ajax({
			method: "GET",
			url: "http://chezmeme.com/sso/api/v1.5/users/" + loginToken + "/" + idConducteur
			})
		.done(function(data){
			if(idConducteur == idUser)
				{
				$('#planning .col[data-date="' + noAnnee + '-' + noMois + '-' + noJour + '"] [data-demiJ="' + idDemiJ + '"] .data').html(data[0].prenom + ' ' + data[0].nom);
				}
				else
				{
				$.ajax({
					method: "GET",
					url: "http://chezmeme.com/sso/api/v1.5/users/" + loginToken + "/" + idUser
					})
				.done(function(dataResa){
					$('#planning .col[data-date="' + noAnnee + '-' + noMois + '-' + noJour + '"] [data-demiJ="' + idDemiJ + '"] .data').html(data[0].prenom + ' ' + data[0].nom + ' (Réservé par ' + dataResa[0].prenom + ' ' + dataResa[0].nom + ')');
					});
				}
			});
		demiJ.data = idConducteur;
		}
	}
	
function loadVehiculesList() {
	$.ajax({
		method: "GET",
		url: urlAPIVoiture + "vehicules/" + loginToken
		})
	.done(function(data){ 
        if(detectError(data))
            {
            return;
            }
		$('#vehicules').html('');
		data.forEach(function (e) {
			$('#vehicules').append('<option value="' + e.immmatriculation + '">' + e.marque + ' ' + e.modele + ' ' + e.couleur + ' (' + e.immmatriculation + ') </option>');
			});
		});
	}