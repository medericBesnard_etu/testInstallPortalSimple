var loginToken = null;

function getToken() {
	return $.cookie("token");
	}
	
function destroyToken() {
	$.removeCookie("token");
	}

$( document ).ready(function() {
	if(getToken() == null)
		{
		document.location.href="index.html"
		}
		else
		{
		loginToken = getToken();
		$.ajax({
			method: "GET",
			url: "http://chezmeme.com/sso/api/v1.5/check_access/" + loginToken + "/vehicules"
			})
		.done(function(data){ 
			if(data.ret == "granted")
				{
				loadVehiculesList();
				}
			if(data.ret == "denied")
				{
				detectError('AU001',$('#LoginPopUp'));
				}
			if(data.ret == "expired")
				{
				detectError('AU003',$('#LoginPopUp'));
				}
			});
		}
		
	$('.loginIcon').click(function() {
		logout();	
		});
		
	$('.textLogin').click(function() {
		logout();
		});
	});
	
function logout() {
	if(loginToken != null)
		{
		$.ajax({
			method: "GET",
			url: "http://chezmeme.com/sso/api/v1.5/logout/" + loginToken
			});
		loginToken = null;
		destroyToken();
		}
	document.location.href="index.html" 		
	}